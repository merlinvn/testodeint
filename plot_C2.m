data= dlmread('cmake-build-debug\\bin\\out.ppq.allpatients.csv');

c2 = transpose(data(:,1:10000)/5.5);

quantile_c2 = quantile(c2, [.25 .5 .75]);

plot(transpose(quantile_c2))