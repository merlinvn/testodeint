//
// Created by nguyentd on 12/19/18.
//
//
#include <vector>
#include <algorithm>
#include <boost/compute.hpp>
#include <iostream>

namespace compute = boost::compute;

int main(const int argc, char** argv) {

  std::cout << "Found " << compute::system::device_count() << " devices:" << std::endl;

  for(auto device: compute::system::devices()){
    std::cout << "    - " << device.name() << " with id: " << device.id();
    std::cout << " (platform: " << device.platform().name() << ")" << std::endl;
  }



  // get the default compute device
  compute::device device = compute::system::devices()[2];

  // print the device's name and platform
  std::cout << "Select default device: " << device.name() << " with id: " << device.id();
  std::cout << " (platform: " << device.platform().name() << ")" << std::endl;

  // setup context
  compute::context context(device);
  compute::command_queue queue(context, device);

//   create data array on host
  int host_data[] = { 1, 3, 5, 7, 9 };

//   create vector on device
  compute::vector<int> device_vector(5, context);

  // copy from host to device
  compute::copy(
      host_data, host_data + 5, device_vector.begin(), queue
  );

  // create vector on host
  std::vector<int> host_vector(5);

  // copy data back to host
  compute::copy(
      device_vector.begin(), device_vector.end(), host_vector.begin(), queue
  );

  return 0;
}

