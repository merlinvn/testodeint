//
// Created by nguyentd on 12/19/18.
//
//#define VEXCL_BACKEND_CUDA


#include <vexcl/vexcl.hpp>
#include <boost/numeric/odeint.hpp>
//[ vexcl_includes
#include <boost/numeric/odeint/external/vexcl/vexcl.hpp>
//]

#include <fstream>

namespace odeint = boost::numeric::odeint;
static const int number_of_params = 13;
static const int number_of_states = 6;

typedef vex::vector<double> vector_type;
typedef vex::multivector<double, number_of_states> state_type;
typedef vex::multivector<double, number_of_params> params_type;


enum parameters {
  F1_indiv,
  F1_thisdose,
  K15,
  K15_thisdose,
  K20,
  K23,
  K24,
  K32,
  K42,
  K56,
  K56_thisdose,
  K62,
  K62_thisdose
};


struct sys_func {
  const params_type &v_prms;

  explicit sys_func(const params_type &_R) : v_prms(_R) {}

  void operator()(const state_type &x, state_type &dxdt, double t) const {
    dxdt(0) = -x(0) * v_prms(K15_thisdose);

    dxdt(1) =
      x(5) * v_prms(K62_thisdose) - x(1) * v_prms(K20) - x(1) * v_prms(K23) + x(2) * v_prms(K32) - x(1) * v_prms(K24) +
      x(3) * v_prms(K42);
    dxdt(2) = x(1) * v_prms(K23) - x(2) * v_prms(K32);
    dxdt(3) = x(1) * v_prms(K24) - x(3) * v_prms(K42);

    dxdt(4) = x(0) * v_prms(K15_thisdose) - x(4) * v_prms(K56_thisdose);
    dxdt(5) = x(4) * v_prms(K56_thisdose) - x(5) * v_prms(K62_thisdose);
  }
};


//void initialize_vprms(std::vector<double> &vector, const int n);

void initialize_parameters_in_device(params_type &params, int size);

void output_state(size_t n, const state_type &X, double time);

void redraw_params_before_newdose(params_type &params, int size, int num_doses_given);

void output_comp2(std::fstream &fs, size_t n, vex::vector<double> &v_comp2, double h);

int main(const int argc, char** argv) {
  std::fstream fs;
  fs.open("out.ppq.allpatients.csv", std::fstream::out);


  using namespace std;
  using namespace odeint;

  // setup the opencl context
  vex::Context ctx(vex::Filter::GPU && vex::Filter::DoublePrecision && vex::Filter::Position(
    1));
  if (!ctx) throw std::runtime_error("No devices available.");

  std::cout << ctx << std::endl;

  // set up number of system, time step and integration time
  const size_t n = 1000000;
  const double dt = 0.01;

  std::vector<double> r(n * number_of_params);

// initialize vprms
//  initialize_vprms(r, n);
  std::cout << "queue r to device" << std::endl;
  params_type params(ctx.queue(), r);

  initialize_parameters_in_device(params, n);

  std::cout << "queue n to device" << std::endl;
  // initialize the state of the lorenz ensemble
  state_type X(ctx.queue(), n);
  X(0) = 0.0;
  X(1) = 0.0;
  X(2) = 0.0;
  X(3) = 0.0;
  X(4) = 0.0;
  X(5) = 0.0;

  double h = 0;
  int num_doses_given = 0;
  while (h <= 28 * 24.0) {
    if (h == 0.0 || h == 24 || h == 48) {
      std::cout << "Time " << h << ": taking drug" << std::endl;
      redraw_params_before_newdose(params, n, num_doses_given);

      X(0) = 3 * 320.0 * 0.577 * params(F1_thisdose);

      num_doses_given++;
    }

    if (((int) round(h)) % 24 == 0) {
//    output_state(n, X, h);
      output_comp2(fs, n, X(1), h);
    }


    // create a stepper
    runge_kutta4<state_type> stepper{};

    if (((int) round(h)) % 24 == 0) {
      std::cout << "Time " << h << ": Solve system" << std::endl;
    }
    // solve the system
    integrate_adaptive(stepper, sys_func(params), X, h, h + 24, dt);
    //]

    h += 24.0;
  }

//  output_state(n, X, h);
  output_comp2(fs, n, X(1), h);


  std::cout << "Finished" << std::endl;

  fs.close();
  return 0;
}

void output_comp2(std::fstream &fs, size_t n, vex::vector<double> &v_comp2, double h) {
  std::vector<double> res(n);
  vex::copy(v_comp2, res);

  for (size_t i = 0; i < n; ++i) {
    fs << res[i] << ",";
  }
  fs << std::endl;
}

void redraw_params_before_newdose(params_type &params, int size, int num_doses_given) {

  vex::RandomNormal<double, vex::random::threefry> rnd;

  // ### first, you don't receive the full dose.  You may receive 80% or 110% of the dose depending
  // on whether you're sitting or standing, whether you've recently had a big meal, whether some gets stuck
  // between your teeth; below, we set the parameter F1 (with some random draws) to adjust this initial dose

  auto IOV_rv = sqrt(0.252) * rnd(vex::element_index(0, size), std::rand());

  // this is the mean relative increase in bioavailability(?) from dose to dose
  double F1D_pe = 0.236;

  // this is the "dose occassion", i.e. the order of the dose (first, second, etc)
  double OCC = 1.0 + (double) num_doses_given; // NOTE the RHS here is a class member

  // THE REASON THIS EXISTS IS THAT DOSE ABSORBTION REALLY DOES INCREASE FOR PATIENTS
  double F1COVD = (1.0 + F1D_pe * (OCC - 1.0));

  // FROM FOSE TO DOSE, **ONLY** IN THE PPQ DATA; THIS MAY NOT OCCUR FOR OTHER DRUGS
  // double THETA8 = 1.0;  // this is just fixed at one
  // double TVF1 = THETA8*F1COVD;
  double TVF1 = F1COVD;

  params(F1_thisdose) = params(F1_indiv) * TVF1 * exp(IOV_rv); // IOV is the between dose variability

  // ### second, you redraw a specific KTR parameter for this dose, using a draw of the variable IOV2
  //     draw a random variate to get the value of the IOV2 variable

  auto IOV2_rv = sqrt(0.195) * rnd(vex::element_index(0, size), std::rand());

  // MT *= exp( IOV2_rv ); - this is what you want to do, but MT goes into the rate variables below as 1/MT
  // so it's simpler to execute the three lines below

  params(K15_thisdose) = params(K15) * exp(-IOV2_rv);
  params(K56_thisdose) = params(K56) * exp(-IOV2_rv);
  params(K62_thisdose) = params(K62) * exp(-IOV2_rv);

}

void output_state(const size_t n, const state_type &X, double time) {
  std::cout << "Hour: " << time << std::endl;

  std::vector<double> res(n * number_of_states);
  vex::copy(X, res);

  for (size_t i = 0; i < 4; ++i) {
    for (int j = 0; j < number_of_states; ++j) {
      std::cout << res[j * n + i] << "\t";

    }
    std::cout << std::endl;
  }
}

void initialize_parameters_in_device(params_type &params, const int size) {
  vex::RandomNormal<double, vex::random::threefry> rnd;


  // ### ### F1_indiv is the relative absorbtion level for this individual

  // before we take into account the effects of dose, the value of TVF1 is one
  double TVF1 = 1.0;
  // this represents between-patient variability
  auto ETA8_rv = sqrt(0.158) * rnd(vex::element_index(0, size), std::rand());

  params(F1_indiv) = TVF1 * exp(ETA8_rv);

  // ### ### KTR is the transition rate among the first three compartments
  const double TVMT_pe = 2.11; // this is the point estimate (_pe) for TVMT;
  auto ETA7_rv = sqrt(0.135) * rnd(vex::element_index(0, size), std::rand());
  auto MT = TVMT_pe * exp(ETA7_rv); // * exp( ETA7_rv );
  const double NN = 2;

  auto KTR = (NN + 1) / MT;
  params(K15) = KTR;
  params(K56) = KTR;
  params(K62) = KTR;


  // ### ### this is the transition rate from the central compt to peripheral compt #1
  const double ACL = 0.75;  // this is the allometric scaling parameter for weight's influence on the Q1 parameter
  const double AV = 1.0;    // this is the allometric scaling parameter for weight's influence on the V2 parameter

  const double THETA2_pe = 2910.0;
  const double THETA3_pe = 310.0;

  //TODO: person weight
//  double Q1 = THETA3_pe * pow( weight/median_weight, ACL );
//  double V2 = THETA2_pe * pow( weight/median_weight, AV );
  double Q1 = THETA3_pe * pow(1, ACL);

  auto ETA2_rv = sqrt(0.371) * rnd(vex::element_index(0, size), std::rand());
  auto V2 = THETA2_pe * pow(1, AV) * exp(ETA2_rv);

  // NOTE ETA3 is fixed at zero; so we do not draw
  // double ETA3_rv = gsl_ran_gaussian( rng, sqrt(0.0) );
  // Q1 *= exp( ETA3 ); // should be zero

  params(K23) = Q1 / V2;



  // ### ### this is the transition rate from to peripheral compt #1 back to the central compt
  //         TVV3 = THETA(4)*(WT/M_WE)**AV;
  //         V3 = TVV3*EXP(ETA(4));
  const double THETA4_pe = 4910.0;

  auto ETA4_rv = sqrt(0.0558) * rnd(vex::element_index(0, size), std::rand());

//  double TVV3 = THETA4_pe * pow( weight/median_weight, AV );
  double TVV3 = THETA4_pe * pow(1, AV);
  auto V3 = TVV3 * exp(ETA4_rv);

  params(K32) = Q1 / V3;


  // ### ###  this is the transition rate from the central compt to peripheral compt #2
  //          Q2 = TVQ2*EXP(ETA(5));
  //          TVQ2 = THETA(5)*(WT/M_WE)**ACL;
  const double THETA5_pe = 105.0;
  auto ETA5_rv = sqrt(0.0541) * rnd(vex::element_index(0, size), std::rand());

//  double TVQ2 = THETA5_pe * pow( weight/median_weight, ACL );
  double TVQ2 = THETA5_pe * pow(1, ACL);

  auto Q2 = TVQ2 * exp(ETA5_rv);
  params(K24) = Q2 / V2;


  // ### ### this is the transition rate from to peripheral compt #2 back to the central compt
  //         V4 = TVV4*EXP(ETA(6));
  //         TVV4 = THETA(6)*(WT/M_WE)**AV;
  const double THETA6_pe = 30900.0;
  auto ETA6_rv = sqrt(0.114) * rnd(vex::element_index(0, size), std::rand());

//  double TVV4 = THETA6_pe * pow( weight/median_weight, AV );
  double TVV4 = THETA6_pe * pow(1, AV);
  auto V4 = TVV4 * exp(ETA6_rv);
  params(K42) = Q2 / V4;

  // ### ### this is the exit rate from the central compartment (the final exit rate in the model
  //         CL = TVCL*EXP(ETA(1));
  //         TVCL = THETA(1)*MF*(WT/M_WE)**ACL;
  const double THETA1_pe = 55.4;
  auto ETA1_rv = sqrt(0.0752) * rnd(vex::element_index(0, size), std::rand());
  const double HILL = 5.51;
  const double EM50 = 0.575;
//  double MF = pow(age,HILL) / ( pow(age,HILL) + pow(EM50,HILL) );
  // TODO: implement age
  double MF = pow(25, HILL) / (pow(25, HILL) + pow(EM50, HILL));

//  double TVCL = THETA1_pe * MF * pow( weight/median_weight, ACL );
  double TVCL = THETA1_pe * MF * pow(1, ACL);
  auto CL = TVCL * exp(ETA1_rv);

  params(K20) = CL / V2;



//  std::cout << " hello" << std::endl;
//  std::vector<double> res(size);
//  vex::copy(params(K15), res);
//  for (int l = 0; l < 5; ++l) {
//    std::cout << res[l] << "\t";
//  }
//  std::cout << std::endl;
//
//  vex::copy(params(K56), res);
//  for (int l = 0; l < 5; ++l) {
//    std::cout << res[l] << "\t";
//  }
//  std::cout << std::endl;
//
//  vex::copy(params(K62), res);
//  for (int l = 0; l < 5; ++l) {
//    std::cout << res[l] << "\t";
//  }
//  std::cout << std::endl;

}
//
//void initialize_vprms(std::vector<double> &v_params, const int n) {
//  for (int j = 0; j < n; ++j) {
//
//    const double TVMT_pe = 2.11; // this is the point estimate (_pe) for TVMT; there is no need to draw a random variate here
//    double MT = TVMT_pe; // * exp( ETA7_rv );
//
//    const double NN = 2;
//    double KTR = (NN + 1) / MT;
//
//    v_params[j + K15 * n] = KTR;
//
////    for (int l = 0; l < number_of_params; ++l) {
//////      std::cout << j * n + l << std::endl;
////      v_params[j * number_of_params + l] = 0.1;
////    }
//  }
//}
