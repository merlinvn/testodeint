include_directories(${PROJECT_SOURCE_DIR}/src)

add_executable(HelloOdeInt main.cpp)

install(TARGETS HelloOdeInt DESTINATION ${PROJECT_SOURCE_DIR}/bin)

